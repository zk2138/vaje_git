import java.util.*;

class Vracilo {
    public static void main(String[] arg) {
        double denar [] = {500, 200, 100, 50, 20, 10, 5, 2, 1, 0.50, 0.20, 0.10, 0.05, 0.02, 0.01};
        Scanner sc = new Scanner(System.in);
        System.out.println("Cena izdelkov je: (v €)");
        double cena = sc.nextDouble();
        System.out.println("Placali boste z: (v €)");
        double placilo = sc.nextDouble();
        System.out.println("Za vracilo: (v €):");
        double vracilo = izracunVracilo(cena, placilo);
        System.out.println(vracilo);
        
        int denarV[] = new int[15];
        denarV = vraciloF(denar, vracilo);
        System.out.println("Vrnjen denar: ");
        izpis(denarV, denar);
    }
    
    public static double izracunVracilo(double cena, double placilo) {
        return (placilo - cena);
    }
    
    public static int[] vraciloF(double[] denar, double vracilo) {
        int [] v = new int [15];
        int i = 0;
        
        while(vracilo > 0 && i < denar.length) {
            if(denar[i] > vracilo) {
                i++;
            } else if(denar[i] <= vracilo) {
                v[i]++;
                vracilo -= denar[i];
            }
        }
        
        return v;
    }
    
    public static void izpis(int[] vracilo, double denar[]) {
        for(int i = 0; i < vracilo.length; i++) {
            if(vracilo[i] > 0) {
                if(denar[i] >= 5) {
                    System.out.println(vracilo[i] + " bankovec/ca po " + denar[i] + " €");
                } else {
                    System.out.println(vracilo[i] + " kovanec/ca po " + denar[i] + " €");
                }
            }
        }
    }
}
